FROM node:15.5.1-alpine3.10

COPY package*.json ./

RUN npm install

COPY . .

RUN apk update
RUN apk add git
RUN apk add yes

RUN git init
RUN git add .
RUN git commit -m "initial"
RUN yarn eject < yes | head -1000
