console.log('prepared to run the test against=' + `http://${process.env.LOCAL_IP}:3000`);
module.exports = {
  silent: false,
  src_folders: [`test`],
  test_settings: {
    default: {
      launch_url: `http://${process.env.LOCAL_IP}:3000`,
      selenium_host: 'selenium-standalone-chrome',
      desiredCapabilities: {
        browserName: 'chrome',
      },
      screenshots: {
        enabled: true,
        on_failure: true,
        path: 'tests_output/screenshots',
      },
    },
    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
      },
    }
  }
}
