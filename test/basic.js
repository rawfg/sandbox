module.exports = {
    'Sample test': function (browser) {  
      browser
        .url(browser.launchUrl)
        .waitForElementVisible('.App-link', 10000)
        .assert.containsText('.App-link', 'Learn React')
        .end();
    },
  }
